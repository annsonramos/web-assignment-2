//function to add a note with fetch
async function addANote(noteTitle, noteBody) {
  const data = { title: noteTitle, note: noteBody }
  const options = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  }
  const response = await fetch('/newNote', options)
  const json = await response.json()
  console.log(json)
}

//function to get all of the notes with fetch
async function getAllNotes() {
  const response = await fetch('/allNotes')
  const json = await response.json()
  console.log(json)
  return await json
}

//function to get one note using a query string
async function getOneNote(noteTitle) {
  const response = await fetch(`/oneNote/?note=${noteTitle}`)
  const json = await response.json()
  console.log(json)
  return await json
}

// example function for creating a p tag to display a note
async function createPTag(noteTitle) {
  const noteDiv = document.querySelector('.notes')
  const note = await getOneNote(noteTitle)
  const newNote = `<p>${note[0].note}</p>`
  noteDiv.innerHTML = newNote
}

//add a note example
const title = "note2"
const note = "this is a sample note"

//addANote(title, note) //uncomment to test add a note

//get one note and add it to a paragraph example
const titleOfNote = "note2"
createPTag(titleOfNote)
