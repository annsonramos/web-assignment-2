# Getting Set up

Starter files for ACIT 1620 assignment 2 winter 2020

Please don't alter the file structure or change any of the file names.

Edit the index.html and style.css files in the public directory.

This application uses nedb for a local database. It is no longer maintained but is an ok tool for testing a locally hosted database
[nedb](https://github.com/louischatriot/nedb)

## Fresh database

If you want to start from scratch with your database you can either delete the database.db file or open it in VSCode and delete its contents.

## Starting the server

to start the server `cd` into the 'assignment2' directory and run `node index.js` then go to http://localhost:3000
